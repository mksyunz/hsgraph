# By default, GHC will try to link random against MacPorts version of libiconv and fails.
# So, we make sure it searches system libraries first for dependencies.
# I'd like to link explicitly against /usr/lib/libiconv but there doesn't seem to be a 
# way to specify that to GHC. This is the Internet Approved solution.
random: random.hs
	ghc --make -L/usr/lib random.hs

