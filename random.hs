import Graph
import System.Random
import Control.Monad
import Control.Monad.MC
import qualified Data.Map as M
import qualified Control.Monad.Loops as ML
import Data.GraphViz
import Data.GraphViz.Printing(renderDot, toDot)
import qualified Data.Text.Lazy as T

minVertices = 4
maxVertices = 10
minDegree = 3
maxDegree = 4

randomGenIO :: IO (UndirectedGraph)
randomGenIO = do
    numVertices <- randomRIO (minVertices, maxVertices)
    edges <- foldM (createEdges numVertices) [] $ take numVertices (iterate (+1) 0)
    return $ M.fromList edges
    where
        createEdges numVertices prevAdj vertex =  do
            degree <- randomRIO (minDegree, maxDegree)
            let nhbrList = fst $ runMC (sampleIntSubset numVertices degree) 
                    $ mt19937 (fromIntegral degree)
            return $ (vertex, nhbrList):prevAdj
    
toDotGraph:: UndirectedGraph -> DotGraph Int
toDotGraph g 
    = DotGraph
    { strictGraph = True
    , directedGraph = False
    , graphID = Nothing
    , graphStatements 
        = DotStmts
        { attrStmts = []
        , subGraphs = []
        , nodeStmts =  map (\k -> DotNode k []) (M.keys g)
        , edgeStmts = concatMap (uncurry vertEdges) (M.toList g)
        }
    }
vertEdges v = foldl (\es d -> (DotEdge v d []):es) []
main = randomGenIO >>= putStrLn . T.unpack . renderDot . toDot . toDotGraph

