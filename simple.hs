data G = G [Int] [(Int, [Int])] deriving (Show)

-- Given a graph and a secquence of waypoints, 
-- returns all paths that satisfy the waypoints.
waypointPaths:: G -> [Int] -> [[Int]]
waypointPaths g wps = filter (satisfies wps) (allPaths g (head wps)) 

-- Given a path and a sequence of waypoints, returns true of the path visits all of them in order.
satisfies :: [Int] -> [Int] -> Bool
satisfies wps path = 
    case fold_result of
        (Just []) -> True
        otherwise -> False
    where
        fold_result = foldl acc (Just path) wps
        acc Nothing wp = Nothing
        acc (Just path') wp = let 
                new = dropWhile (wp /=) path'
            in
                case new of
                    [] -> Nothing
                    otherwise -> (Just $ tail new)


-- Given a graph and a starting vertex, generate all possible paths from this vertex.
allPaths (G _ es) v | (all ((v /=) . fst) es) = []
allPaths g v | (out g v) == [] = [[v]]
allPaths g v = [v]:(map (v:) (concat (map (allPaths (remove g v)) (out g v))))

-- Given a graph and a vertex, return all the vertices that can be
-- reached from the given vertex.
out (G _ es) v = snd . head $ dropWhile (fst_not v) es

-- Given a graph and a vertex, removes that vertex from the graph.
remove :: G -> Int -> G
remove (G vs es) v = 
    let
        not_v =  filter (v /=) 
    in 
        G (not_v vs) 
            (map (\(h, t) -> (h, not_v t)) (filter (fst_not v) es))

-- Utility method that takes a tuple and returns true if its
-- first element is not v.
fst_not v = (v /=) . fst

    
