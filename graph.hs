module Graph where 
import qualified Data.Vector.Unboxed as V
import qualified Data.Map as M
import qualified Data.List as L

type Vertex = Int
type Neighbors = [Vertex]
type UndirectedGraph = M.Map Vertex Neighbors
type Path = [Vertex]

-- Given: a graph G(V, E) and a list of vertices W.
-- Return: a path through G such that all vertices in W are visited in order.
generateAllPaths :: UndirectedGraph -> Path -> [Path]
generateAllPaths g path = 
    if (all (uncurry $ hasEdge g) edges)
    then [path]
    else concatMap 
            (generateAllPaths (removeFixedElements path g))  
            (map dedup (sequence $ concatMap (do_edge g) edges))
        where
            edges = zip path $ drop 1 path
            dedup = foldr f [] 
            f k []= [k]
            f k prev@(x:xs) = if x == k then  prev else k:prev
            do_edge g (i, j) = 
                if (hasEdge g i j) 
                    then [[i],[j]]
                    else [[i], neighbors g i, neighbors g j, [j]]

            
hasEdge :: UndirectedGraph -> Vertex -> Vertex -> Bool
hasEdge g f t = elem t $ neighbors g f

neighbors g v = (M.!) g v

-- Given a path and a graph, remove all edges in the path from the graph as well 
-- as remove from the graph all vertices that have both in and out edges in the path.
removeFixedElements:: Path -> UndirectedGraph -> UndirectedGraph
removeFixedElements path g = foldl removeFixed g path'
    where 
        path' = zip3 pre just_nodes post
        pre = Nothing:just_nodes
        post = just_nodes ++ [Nothing]
        just_nodes = map Just $ path
        contains = hasEdge g
        removeFixed g' (Nothing, Just v, Just o) =  removeEdge g' v o
        removeFixed g' (Just i, Just v, Nothing) = removeEdge g' i v
        removeFixed g' (Just i, Just v, Just o) 
            | contains i v && contains v o  = removeVertex g' v
            | contains i v = removeEdge g' i v
            | contains v o = removeEdge g' v o
            | otherwise = g'

removeEdge :: UndirectedGraph -> Vertex -> Vertex -> UndirectedGraph
removeEdge g f t = M.adjust (not t) f $ M.adjust (not f) t g
    where not i = filter (i /=)

removeVertex :: UndirectedGraph -> Vertex -> UndirectedGraph
removeVertex n v = M.map remove_v $ M.delete v n
    where remove_v = filter (v /=)
        
-- Algorithm end
addVertex n = M.insert (M.size n) [] n

addEdge :: UndirectedGraph -> Vertex-> Vertex -> UndirectedGraph
addEdge n f t = M.adjust (L.insert t) f $ M.adjust (L.insert f) t n

-- Maximum number of edges of a graph of i verticesm
maxEdges :: Int -> Int
maxEdges i = i*(i-1) `div` 2

